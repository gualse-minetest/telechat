# How to use

1. Create new bot in t.me/botfather
1. Add bot to your server group
1. Make your group temporarily private
1. Copy link to any message
1. Remember numbers between slashes in the link. It's your chat id. (for example you need remember 1404938324 if your link is https://t.me/c/1404938324/6)
1. Return your group public if you want
1. Open minetest.conf in your server directory (or all setting if you host from client)
1. Set telechat_chat_id to -100`your chat id` (example: telechat_chat_id=-1001404938324)
1. Set telechat_bot_token to your bot token from last botfather message (example: telechat_bot_token=1145595677:AAGzf-am3JQqmlHurYDlhiBs8jc3S4QiI_g)
1. Add telechat to secure.http_mods (example: secure.http_mods=telechat or secure.http_mods=skinsdb,telechat,my_another_internet_mod)

You also can send messages to private messages. Use t.me/getmyid_bot and set telechat_chat_id to bot response. (telechat_chat_id=451855494)